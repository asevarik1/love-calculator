import React from 'react';
import { View,Text,StyleSheet } from 'react-native';


const DisplayLove = (props)=>{
console.log(props)

if(props.data.message){
    return <Text style={styles.text}>Sorry Something Went Wrong</Text>
}
if (props.data.percentage=='0'){
    return <Text style={styles.text}>No Match</Text>
}
    return(
    <View style={styles.container}>
        <Text style={styles.text}>{props.data.fname}&{props.data.sname}</Text>
        <Text style={styles.text}> {props.data.percentage}%</Text>
        <Text style={styles.text}>{props.data.result}</Text>
    </View>
)

}




export default DisplayLove

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#fff',
      justifyContent:'center',
      marginTop :90,
      backgroundColor:'transparent'
    },
    text:{
        fontSize:30,
        textAlign:'center',
        fontWeight:'bold',
        marginTop:10,
        color:'#F8D3E6'
    }
  });
  