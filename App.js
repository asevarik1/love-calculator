import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, View ,ImageBackground,Image,KeyboardAvoidingView} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { TextInput,Appbar,Button,Text } from 'react-native-paper';
import DisplayLove from './Components/DisplayLove'

submitit=(firstperson,secondperson,setresult)=>{
    fetch(`https://love-calculator.p.rapidapi.com/getPercentage?fname=${firstperson}&sname=${secondperson}`, {
	"method": "GET",
	"headers": {
		"x-rapidapi-host": "love-calculator.p.rapidapi.com",
		"x-rapidapi-key": "457e882c75mshb3addec6b0926c8p10f89fjsn57cb42b0d0f5"
	}
})
.then(response =>response.json())
.then(data=>{
  console.log(data)
  setresult(data)
})
.catch(err => {
	console.log(err);
 });
}
  
 function App() {
  const [firstperson,setfirstperon]=React.useState('')
  const [secondperson,setsecondperon]=React.useState('')
  const [result,setresult]=React.useState({percentage:'calculated percentage',fname:'ABC',sname:'XYZ',result:'Get your Feedback'})
  return (
    <View style={styles.container}>
       <KeyboardAvoidingView  
        behavior='height'keyboardVerticalOffset={-550}>  

       <Appbar.Header style={{alignItems:"center",backgroundColor:'#5260cb'}}>
      <Appbar.Content title="Love Calculator" style={{...styles.header}} />
    </Appbar.Header>
    <ImageBackground source={require('./assets/bg1.jpg')} style={{width:'100%',height:'100%'}}>
    <Image source={require('./assets/heart.png')}style={{height:130,width:130,alignSelf:'center'}}/>
    <TextInput
      label="firstperson"
      placeholder={firstperson}
      onChangeText={text => setfirstperon(text)}
      mode='flat'
      underlineColor={'white'}
      style={styles.textinput}
      theme={{ colors: {
        placeholder: 'white', text: 'white', primary: 'white',
        underlineColor: 'transparent', background: '#003489'
}}}
    />
       <TextInput
      label="secondperson"
      placeholder={secondperson}
      onChangeText={text => setsecondperon(text)}
      underlineColor={'white'}
      style={styles.textinput}
      underlineColor={'white'}
      style={styles.textinput}
      theme={{ colors: {
        placeholder: 'white', text: 'white', primary: 'white',
        underlineColor: 'transparent', background: '#003489'
}}}
    />
    <Button icon="anchor"  mode="contained" style={{margin:30,borderRadius:30,backgroundColor:'transparent',alignItems:'center'}}onPress={() =>submitit(firstperson,secondperson,setresult)}>Calculate</Button>
    <DisplayLove data={result} />
    <Text style={{fontSize:20,textAlign:'center',color:'#F8D3E6',marginTop:80}}>Developed By:Ahishek Sevarik</Text>
    </ImageBackground>
    <StatusBar style="auto" />
    </KeyboardAvoidingView>
    </View>
  );
}
export default App
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#5655cd'
  },
  textinput:{
  margin:20,
  backgroundColor:'transparent',
  color:'white'
},
header:{
fontSize:30,
alignItems:'center'

}
});
